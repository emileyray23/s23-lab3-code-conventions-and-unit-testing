package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class ThreadControllerTests {
    private static ThreadController threadController;
    private static Thread thread;
    private static Post post;
    private static Vote vote;
    private static MockedStatic<ThreadDAO> threadMock;
    private static MockedStatic<UserDAO> userMock;

    @BeforeAll
    public static void setup() {
        threadController = new ThreadController();

        // Create a sample thread
        thread = new Thread();
        thread.setId(1);
        thread.setCreated(Timestamp.valueOf(LocalDateTime.now()));
        thread.setSlug("test-slug");
        thread.setForum("test-forum");
        thread.setMessage("test-message");
        thread.setTitle("test-title");
        thread.setVotes(2);

        // Create a sample post
        post = new Post();
        post.setId(1);
        post.setAuthor("test-author");
        post.setForum("test-forum");
        post.setThread(1);
        post.setCreated(Timestamp.valueOf(LocalDateTime.now()));

        // Create a sample vote
        vote = new Vote("test-nickname", 1);
        vote.setTid(1);

        userMock = mockStatic(UserDAO.class);
        threadMock = mockStatic(ThreadDAO.class);

        userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());
        threadMock.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(thread);
        threadMock.when(() -> ThreadDAO.change(any(), anyInt())).thenReturn(2);
        threadMock.when(() -> ThreadDAO.getThreadById(anyInt())).thenReturn(thread);
    }

    @Test
    @DisplayName("Successfully creates post")
    void correctlyCreatesPost() {
        ResponseEntity result = threadController.createPost("test-slug", List.of(post));

        assertEquals(HttpStatus.CREATED, result.getStatusCode());
        assertEquals(List.of(post), result.getBody());
    }

    @Test
    @DisplayName("Successfully creates vote")
    void correctlyCreatesVote() {
        ResponseEntity result = threadController.createVote("123", vote);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(thread, result.getBody());
    }

    @Test
    @DisplayName("Get thread info")
    void correctlyGetThreadInfo() {
        ResponseEntity result = threadController.info("test-slug");

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(thread, result.getBody());
    }

    @Test
    @DisplayName("Change thread details")
    void correctlyChangeThreadInfo() {
        ResponseEntity result = threadController.change("test-slug", thread);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(thread, result.getBody());
    }
}